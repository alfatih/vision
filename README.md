Al-Fatih Vision
===============

Al-Fatih is the hexapod fire fighting robot of Gadjah Mada Robotic Team, Gadjah Mada University, Indonesia. This package is Al-Fatih Vision, which enables the robot to see it's environment.