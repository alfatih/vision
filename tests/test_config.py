import unittest
import ConfigParser
import cv2


class TestConfiguration(unittest.TestCase):
    """ Test the configuration file """

    def setUp(self):
        """ Setup the configuration object """
        self.config = ConfigParser.SafeConfigParser()
        self.files = self.config.read('vision/settings.cfg')

    def test_config_open(self):
        """ Test if the config file is available """
        self.assertTrue(len(self.files) > 0)

    def test_capture_value(self):
        """ Test the capture values in the config file """
        capture_source = self.config.get('Capture', 'source')
        capture_index = self.config.get('Capture', 'index')
        camera_angle = self.config.getint('Capture', 'camera_angle')
        frame_height = self.config.getint('Capture', 'frame_height')
        frame_width = self.config.getint('Capture', 'frame_width')

        self.assertIn(capture_source, ('camera', 'file'))
        self.assertTrue(camera_angle > 0)
        self.assertTrue(camera_angle < 180)
        
        if capture_source == 'camera':
            self.assertTrue(int(capture_index) >= 0)
        else:
            self.assertTrue(len(capture_index) > 0)

    def test_threshold_value(self):
        """ The the threshold values in the config file """
        environment = self.config.get('Threshold', 'environment')
        threshold_r = self.config.getint('Threshold', 'color')
        threshold_y = self.config.getint('Threshold', 'luminance')

        self.assertIn(environment, ('normal', 'bright', 'dark'))

        self.assertTrue(threshold_r > 0)
        self.assertTrue(threshold_r < 256)

        self.assertTrue(threshold_y > 0)
        self.assertTrue(threshold_y < 256)
