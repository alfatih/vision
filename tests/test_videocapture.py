import unittest

from vision.videocapture import VideoCapture


class TestVideoCapture(unittest.TestCase):
    """ Test the VideoCapture Class """

    def test_camera_initialization(self):
        """ Test Camera Initialization

        This test check if camera is connected and accessiable.
        """
        device = "nocam"
        self.assertRaises(RuntimeError, VideoCapture, device)

        device = 1
        try:
            cam = VideoCapture(device)
            self.assertTrue(cam.is_available())
            cam.close()

        except RuntimeError:
            pass

    def test_frame_size(self):
        """ Test camera frame size """
        device = 1

        try:
            cam = VideoCapture(device)
            weight, height = cam.get_frame_size()
            self.assertEqual(weight, 640)
            self.assertEqual(height, 480)
            cam.close()

        except RuntimeError:
            pass

    def test_white_balance(self):
        """ Test camera white balance access """
        device = 1
        try:
            cam = VideoCapture(device)
            white_balance = 6000
            cam.set_white_balance(white_balance)
            self.assertEqual(cam.get_white_balance(), white_balance)
            cam.close()
        except RuntimeError:
            pass
