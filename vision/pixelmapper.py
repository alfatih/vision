import math


class PixelMapper:
    """ Pixel Mapper

    The PixelMapper is a class to determine the direction angle from a pixel
    coordinate in the frame.
    """

    def __init__(self, width, height, angle):
        """ Initialize PixelMapper class """
        self.width = width
        self.height = height
        self.angle = angle

    def set_frame(self, width, height):
        """ Set frame size """
        self.width = width
        self.height = height

    def set_camera_angle(self, angle):
        """ Set camera angle """
        self.angle(angle)

    def get(self, center):
        """ Get direction angle from frame position """
        frame_center = self.width / 2
        object_center = frame_center - center
        direction = math.ceil(((frame_center - object_center) * self.angle) / self.width) - 90

        return direction
