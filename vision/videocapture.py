import cv2


class VideoCapture:
    """ Video Capture Class """
    def __init__(self, index):
        """ Vide Capture Initialization

        This method initialize video stream, either from camera stream or
        vide file.

        Parameters
        ----------
        index : int or stream
                camera index of video file path
        """
        self.capture = cv2.VideoCapture(index)

        if not self.capture.isOpened():
            raise RuntimeError('Video stream not found')

        self.set_frame_size(640, 480)

    def is_available(self):
        """ Check if camera is initialized """
        return self.capture.isOpened()

    def set_frame_size(self, width, height):
        """ Set camera frame size

        Parameters
        ----------
        width : frame width
        height : frame height
        """
        self.capture.set(cv2.CAP_PROP_FRAME_WIDTH, width)
        self.capture.set(cv2.CAP_PROP_FRAME_HEIGHT, height)

    def get_frame_size(self):
        """ Get camera frame size

        Returns
        ------
        width : frame width
        height : frame height
        """
        width = self.capture.get(cv2.CAP_PROP_FRAME_WIDTH)
        height = self.capture.get(cv2.CAP_PROP_FRAME_HEIGHT)

        return width, height

    def get_white_balance(self):
        """ Get camera white balance value """
        return self.capture.get(cv2.CAP_PROP_WHITE_BALANCE_BLUE_U)

    def set_white_balance(self, value):
        """ Set camera white balance value """
        self.capture.set(cv2.CAP_PROP_WHITE_BALANCE_BLUE_U, value)

    def read(self):
        """ Read frame from camera """
        self.frame = self.capture.read()[1]
        return self.frame

    def close(self):
        """ Release camera """
        self.capture.release()
