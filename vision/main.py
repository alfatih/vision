import cv2
import time
import ConfigParser

from videocapture import VideoCapture
from firedetector import FireDetector
from pixelmapper import PixelMapper

def vision_setup(config):
    """ Setting up the vision package

    Prepare the capture instance and the fire and direction detector
    """
    camera = config.getint('Capture', 'index')
    width = config.getint('Capture', 'frame_width')
    height = config.getint('Capture', 'frame_height')
    angle = config.getint('Capture', 'camera_angle')

    cam = VideoCapture(camera)
    fire = FireDetector()
    direction = PixelMapper(width, height, angle)
    
    return cam, fire, direction

def get_fps(fps, start):
    """ Count FPS rate """
    fps += 1
    end = time.clock()

    if end - start > 1:
        print fps, 'fps'
        fps = 0
        start = time.clock()

    return fps, start

def main():
    """ Al Fatih Vision Main Function """
    config = ConfigParser.SafeConfigParser()
    config.read('vision/settings.cfg')

    cam, fire, direction = vision_setup(config)

    fps = 0
    start = time.clock()

    while True:
        frame = cam.read()

        rth = config.getint('Threshold', 'color')
        yth = config.getint('Threshold', 'luminance')
        x, y, w, h = fire.find(frame, yth, rth)

        angle = direction.get(x+(w/2))
        print angle

        if w > 0:
            cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)

        cv2.imshow('Frame', frame)

        fps, start = get_fps(fps, start)

        k = cv2.waitKey(1) & 0xff
        if k == ord('q'):
            break