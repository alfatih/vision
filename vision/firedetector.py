import cv2
import numpy as np


class FireDetector:
    """ Fire Detector Class """

    def filter_intensity(self, frame, threshold_y):
        """ Filter fire by it's intensity

        Most of the time, fire is the brightest object in the frame. This
        method utilize that property to extract the fire.

        Parameters
        ----------
        frame : the frame to be filtered
        threshold_y : Y component threshold
        """
        ycrcb = cv2.cvtColor(frame, cv2.COLOR_BGR2YCrCb)
        threshold_low = np.array([threshold_y, 0, 0])
        threshold_high = np.array([255, 240, 240])

        self.intensity_mask = cv2.inRange(ycrcb, threshold_low, threshold_high)

        return self.intensity_mask

    def filter_color(self, frame, threshold_r):
        """ Filter fire by it's color

        Fire can be filtered by it's reddish color property. This method
        utilize that property to extract the fire.

        Parameters
        ----------
        frame : the frame to be filtered
        threshold_r : red component threshold
        """
        threshold_low = np.array([0, 0, threshold_r])
        threshold_high = np.array([threshold_r-20, threshold_r-10,
                                   255])

        self.color_mask = cv2.inRange(frame, threshold_low, threshold_high)

        return self.color_mask

    def extract(self, frame, threshold_y, threshold_r):
        """ Extract fire from original frame

        This method create a new mask, extracting only fire from the original
        frame.

        Parameters
        ----------
        frame : the frame to be filtered
        threshold_y : Y component threshold
        threshold_r : red component threshold
        """
        self.filter_intensity(frame, threshold_y)
        self.filter_color(frame, threshold_r)

        kernel = np.ones((5, 5), np.uint8)
        intensity_mask = cv2.dilate(self.intensity_mask, kernel, iterations=1)

        kernel = np.ones((10, 10), np.uint8)
        color_mask = cv2.dilate(self.color_mask, kernel, iterations=1)

        self.fire_mask = cv2.bitwise_and(color_mask, color_mask,
                                         mask=intensity_mask)

        return self.fire_mask

    def selection(self):
        """ Select real fire from fire candidates

        This method do selection for the best fire candidate and return
        it's bounding rectangle coordinate.
        It requires the extract method to be called first.
        """
        im2, contours, hierarchy = cv2.findContours(self.fire_mask.copy(),
                                                    cv2.RETR_EXTERNAL,
                                                    cv2.CHAIN_APPROX_SIMPLE)

        x, y, w, h = 0, 0, 0, 0
        if len(contours) > 0:
            fire_candidates = np.zeros((len(contours), 2))

            for i, c in enumerate(contours):
                fire_candidates[i] = [i, cv2.contourArea(c)]

            fire_candidates = np.sort(fire_candidates, 0)
            fire = fire_candidates[len(fire_candidates)-1, 0]
            fire_contour = contours[int(fire)]

            if cv2.contourArea(fire_contour) > 100:
                x, y, w, h = cv2.boundingRect(fire_contour)

        return x, y, w, h

    def find(self, frame, threshold_y, threshold_r):
        """ Find fire in frame

        Abstraction of all algorithm to find fire in a frame. It returns
        the x and y cordinate of the fire's bounding box and also it's
        width and height.

        Parameters
        ----------
        frame : the frame to be filtered
        threshold_y : Y component threshold
        threshold_r : red component threshold
        """
        self.extract(frame, threshold_y, threshold_r)
        return self.selection()

    def show_masks(self, color=False, intensity=False, fire=False):
        """ Display color, intensity and fire mask """
        if color:
            cv2.imshow("Color Mask", self.color_mask)

        if intensity:
            cv2.imshow("Intensity Mask", self.intensity_mask)

        if fire:
            cv2.imshow("Fire Mask", self.fire_mask)
